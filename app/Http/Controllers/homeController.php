<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $cat = DB::table('product_categories')->select('*');
        $cat = $cat->get();

        $pro = DB::table('products')->select('*');
        $pro = $pro->get();

        return view('home', compact('pro','cat'));
    }
}
