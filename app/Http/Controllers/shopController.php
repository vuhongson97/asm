<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class shopController extends Controller
{
    //
    public function index()
    {
        $cat = DB::table('product_categories')->select('*');
        $cat = $cat->get();
        $brands = Brand::all();

        if (request()->category) {
            if (request()->brand) {
                $pros = DB::table('products')->select('*')->where([
                    ['catalog', request()->category],
                    ['brand', request()->brand]
                ]);

                $count = DB::table('products')->where([
                    ['catalog', request()->category],
                    ['brand', request()->brand]
                ])->count();
            } else {
                $pros = DB::table('products')->select('*')->where('catalog', request()->category);

                $count = DB::table('products')->where('catalog', request()->category)->count();
            }
            $categoryName = $cat->where('id', request()->category)->first()->name;
        } else {
            if (request()->brand) {
                $pros = DB::table('products')->where('brand', request()->brand);
                $count = DB::table('products')->where('brand', request()->brand)->count();
                $categoryName=$brands->where('id',request()->brand)->first()->name;
            } else {
                $pros = DB::table('products');
                $count = DB::table('products')->count();
                $categoryName = "PRODUCTS";
            }
        }

        if (request()->sort == "H_L") {
            $pros = $pros->orderBy('price', 'desc')->paginate(9);
        } elseif (request()->sort == "L_H") {
            $pros = $pros->orderBy('price')->paginate(9);
        } else {
            $pros = $pros->paginate(9);
        }


        return view('shop.shop', compact('pros', 'cat', 'count', 'brands'))->with([
            'categoryName' => $categoryName,
            'pros' => $pros

        ]);
    }


    public function detail($id, $name)
    {

        $pro = Product::where('id', '=', $id)->select('*')->first();
        $cat = ProductCategory::where('id', '=', $pro->catalog)->select('*')->first();
        $brand = Brand::where('id', '=', $pro->brand)->select('*')->first();
        return view('shop.detail', compact('pro', 'cat', 'brand'));
    }

    public function search(request $request)
    {

        $query = $request->input('search');

        $pros = DB::table('products')->where('name', 'LIKE', "%$query%")->paginate(9);

        $cat = DB::table('product_categories')->select('*');
        $cat = $cat->get();
        $brands = Brand::all();
        return view('shop.search')->with([
            'pros' => $pros,
            'cat' => $cat,
            'brands' => $brands,
        ]);
    }
}
