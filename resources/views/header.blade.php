<!-- ##### Header Area Start ##### -->
<header class="header_area">
    <div class="classy-nav-container breakpoint-off d-flex align-items-center justify-content-between">
        <!-- Classy Menu -->
        <nav class="classy-navbar" id="essenceNav">
            <!-- Logo -->
            <a class="nav-brand" href="/"><img src="{{Voyager::image(setting('site.logo'))}}" alt=""></a>
            <!-- Navbar Toggler -->
            <div class="classy-navbar-toggler">
                <span class="navbarToggler"><span></span><span></span><span></span></span>
            </div>
            <!-- Menu -->
            <div class="classy-menu">
                <!-- close btn -->
                <div class="classycloseIcon">
                    <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                </div>
                <!-- Nav Start -->
                <div class="classynav">
                    <ul>
                        {{menu('site','nav')}}


                    </ul>
                </div>
                <!-- Nav End -->
            </div>
        </nav>

        <!-- Header Meta Data -->
        <div class="header-meta d-flex clearfix justify-content-end">
            <!-- Search Area -->
            <div class="search-area">
            <form action="{{route('search')}}" method="get">

            <input type="search" name="search" id="headerSearch" value="{{request()->input('search')}}" placeholder="Type for search">
                    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
            </div>
            <!-- Favourite Area -->
            {{-- <div class="favourite-area">
                <a href="#"><img src="{{asset('/essence/')}}/img/core-img/heart.svg" alt=""></a>
            </div> --}}
            <!-- User Login Info -->
            @guest
            <div class="user-login-info">
                <a href="{{ route('login') }}"><img src="{{asset('/essence/')}}/img/core-img/user.svg" alt=""></a>
            </div>
                @else
                <div class="user-login-info">
                    <a href="{{ route('profile') }}"><img src="{{asset('/essence/')}}/img/core-img/user.svg" alt="">{{ Auth::user()->name }}</a>

                </div>
                <div class="user-login-info">
                    <a href="{{ route('logout') }}"   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            @endguest
            <!-- Cart Area -->
            <div class="cart-area">
                <a href="#" id="essenceCartBtn"><img src="{{asset('/essence/')}}/img/core-img/bag.svg" alt=""> <span>
                         @if(Cart::count()>0)
                            {{Cart::content()->count()}}
                        @else
                            0
                        @endif
                    </span></a>
            </div>
        </div>

    </div>
</header>
<!-- ##### Header Area End ##### -->

<!-- ##### Right Side Cart Area ##### -->
<div class="cart-bg-overlay"></div>

<div class="right-side-cart-area">

    <!-- Cart Button -->
    <div class="cart-button">
        <a href="#" id="rightSideCart"><img src="{{asset('/essence/')}}/img/core-img/bag.svg" alt=""> <span>
                @if(Cart::count()>0)
                    {{Cart::content()->count()}}
                @else
                    0
                @endif
            </span></a>

    </div>

    <div class="cart-content d-flex">

        <!-- Cart List Area -->
        <div class="cart-list">
            <!-- Single Cart Item -->
            @if(Cart::count()>0)
                @foreach(Cart::content() as $item)
                    @php
                        $img=json_decode($item->model->image)
                    @endphp
                    <div class="single-cart-item">
                        <a href="/delete/{{$item->rowId}}" class="product-image">
                            <img src="{{Voyager::image($img[0])}}" class="cart-thumb" alt="">
                            <!-- Cart Item Desc -->
                            <div class="cart-item-desc">
                                <span class="product-remove"><i class="fa fa-close" aria-hidden="true"></i></span>
                                <span class="badge">Mango</span>
                                <h6>{{$item->model->name}}</h6>
                                <p class="size">Qty: {{$item->qty}}</p>

                                <p class="price">
                                    @if($item->model->discount>0)
                                        @php
                                            $newprice=$item->model->price -($item->model->price*($item->model->discount/100))
                                        @endphp
                                        {{number_format($newprice)}}
                                    @else
                                        {{number_format($item->model->price)}}
                                    @endif
                                </p>
                            </div>
                        </a>
                    </div>
                @endforeach
            <!-- Single Cart Item -->
            @else
                <h5 class="ml-4 mt-5">empty cart item</h5>
            @endif


        </div>

        <!-- Cart Summary -->
        <div class="cart-amount-summary">

            <h2>Summary</h2>
            <ul class="summary-table">
                <li><span>subtotal:</span> <span>{{Cart::subtotal(0)}}</span></li>
                <li><span>delivery:</span> <span>Free</span></li>
                <li><span>tax:</span> <span>{{Cart::tax(0)}}</span></li>
                <li><span>discount:</span> <span>-15%</span></li>
                <li><span>total:</span> <span>{{Cart::total(0)}}</span></li>
            </ul>
            <div class="checkout-btn mt-100">
                <a href="/cart" class="btn essence-btn">View Cart</a>
            </div>
        </div>
    </div>
</div>
<!-- ##### Right Side Cart End ##### -->
