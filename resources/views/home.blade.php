@extends('layouts.master')
@section('title','Home')
@section('content')
    <!-- ##### Welcome Area Start ##### -->
    <section class="welcome_area bg-img background-overlay" style="background-image: url({{Voyager::image(setting('site.banner'))}});
        background-size: contain ">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
{{--                    <div class="hero-content">--}}
{{--                        <h6>asoss</h6>--}}
{{--                        <h2>New Collection</h2>--}}
{{--                        <a href="#" class="btn essence-btn">view collection</a>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Welcome Area End ##### -->

    <!-- ##### Top Catagory Area Start ##### -->
    <div class="top_catagory_area section-padding-80 clearfix">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Single Catagory -->
                @foreach($cat as $cat)
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="single_catagory_area d-flex align-items-center justify-content-center bg-img" style="background-image: url({{asset(Voyager::image($cat->image))}})">
                            <div class="catagory-content">
                                <a href="#">{{$cat->name}}</a>
                            </div>
                        </div>
                    </div>
            @endforeach
            <!-- Single Catagory -->



            </div>
        </div>
    </div>
    <!-- ##### Top Catagory Area End ##### -->

    <!-- ##### CTA Area Start ##### -->
    <div class="cta-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="cta-content bg-img background-overlay" style="background-image: url({{Voyager::image(setting('site.ads'))}})">
                        <div class="h-100 d-flex align-items-center justify-content-end">
{{--                            <div class="cta--text">--}}
{{--                                <h6>-60%</h6>--}}
{{--                                <h2>Global Sale</h2>--}}
{{--                                <a href="#" class="btn essence-btn">Buy Now</a>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### CTA Area End ##### -->

    <!-- ##### New Arrivals Area Start ##### -->
    <section class="new_arrivals_area section-padding-80 clearfix">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading text-center">
                        <h2>Popular Products</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="popular-products-slides owl-carousel">
                    @foreach($pro as $pro)

                        <!-- Single Product -->
                            <div class="single-product-wrapper">
                                <a href="/detail/{{$pro->id}}/{{covert_vi_to_en($pro->name)}}">
                                    <!-- Product Image -->
                                    <div class="product-img">
                                        @php $pic=json_decode($pro->image)@endphp
                                        <img src="{{asset(Voyager::image($pic[0]))}}" alt="{{$pro->name}}">
                                    @if($pro->discount>0)
                                        <!-- Product Badge -->
                                            <div class="product-badge offer-badge">
                                                <span>-{{$pro->discount}}%</span>
                                            </div>
                                    @endif
                                        <!-- Hover Thumb -->
                                        <img class="hover-img" src="{{asset(Voyager::image($pic[1]))}}" alt="{{$pro->name}}">
                                        <!-- Favourite -->
                                        <div class="product-favourite">
                                            <a href="#" class="favme fa fa-heart"></a>
                                        </div>
                                    </div>
                                    <!-- Product Description -->
                                    <div class="product-description">
                                        <span>topshop</span>
                                        <a href="/detail/{{$pro->id}}/{{covert_vi_to_en($pro->name)}}">
                                            <h6>{{$pro->name}}</h6>
                                        </a>
                                        @if($pro->discount>0)
                                            @php
                                                $newprice=$pro->price-($pro->price*($pro->discount/100))
                                            @endphp
                                            <p class="product-price"><span
                                                    class="old-price">{{number_format($pro->price)}}</span> {{number_format($newprice)}}
                                            </p>
                                            @php
                                                $price=$newprice
                                            @endphp
                                        @else
                                            <p class="product-price"> {{number_format($pro->price)}}
                                            </p>
                                        @php
                                            $price=$pro->price
                                        @endphp
                                    @endif

                                        <!-- Hover Content -->
                                        <div class="hover-content">
                                            <!-- Add to Cart -->
                                            <div class="add-to-cart-btn">
                                                <form action="{{Route('cart.store')}}" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="id" value="{{$pro->id}}">
                                                    <input type="hidden" name="name" value="{{$pro->name}}">
                                                    <input type="hidden" name="price" value="{{$price}}">
                                                    <button type="submit" class="btn essence-btn">Add to cart</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                        @endforeach






                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### New Arrivals Area End ##### -->

    <!-- ##### Brands Area Start ##### -->
    <div class="brands-area d-flex align-items-center justify-content-between">
        <!-- Brand Logo -->
        <div class="single-brands-logo">
            <img src="{{asset('/essence/')}}/img/core-img/brand1.png" alt="">
        </div>
        <!-- Brand Logo -->
        <div class="single-brands-logo">
            <img src="{{asset('/essence/')}}/img/core-img/brand2.png" alt="">
        </div>
        <!-- Brand Logo -->
        <div class="single-brands-logo">
            <img src="{{asset('/essence/')}}/img/core-img/brand3.png" alt="">
        </div>
        <!-- Brand Logo -->
        <div class="single-brands-logo">
            <img src="{{asset('/essence/')}}/img/core-img/brand4.png" alt="">
        </div>
        <!-- Brand Logo -->
        <div class="single-brands-logo">
            <img src="{{asset('/essence/')}}/img/core-img/brand5.png" alt="">
        </div>
        <!-- Brand Logo -->
        <div class="single-brands-logo">
            <img src="{{asset('/essence/')}}/img/core-img/brand6.png" alt="">
        </div>
    </div>
    <!-- ##### Brands Area End ##### -->
@endsection
