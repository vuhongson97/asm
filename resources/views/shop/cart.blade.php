@extends('layouts.master')
@section('title','checkout')
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-12 col-md-6 col-lg-8 ">
            <div class="order-details-confirmation">

                <div class="cart-page-heading">
                    <h5>Your Order</h5>
                    <p>The Details</p>
                </div>

                <ul class="order-details-form mb-4">
                    <li><span>Product</span><span>qty</span><span>price</span> <span>Action</span></li>
                    @if(Cart::count()>0)
                    @foreach(Cart::content() as $item)
                    <li><span>{{$item->model->name}}</span><span><input type="number" class="form-control w-25 qty" data-id="{{$item->rowId}}" name="qty" value="{{$item->qty}}"></span> <span>  @if($item->model->discount>0)
                                @php
                                    $newprice=$item->model->price -($item->model->price*($item->model->discount/100))
                                @endphp
                                {{number_format($newprice)}}
                            @else
                                {{number_format($item->model->price)}}
                            @endif</span><span><a href="/delete/{{$item->rowId}}"><i class="fa fa-close"></i></a></span></li>
                    @endforeach
                    @else
                        <li><span></span><span>empty cart item</span><span></span></li>
                    @endif

                </ul>


                @if(Cart::count()>0)
                <a href="/empty" class="btn essence-btn">Clear all</a>
                    @endif
                <a href="/shop" class="btn essence-btn-suc ">Continue Shopping</a>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-4 ">
            <div class="order-details-confirmation">

                <div class="cart-page-heading">
                    <h5>Your Order</h5>
                    <p>The Details</p>
                </div>

                <ul class="order-details-form mb-4">

                    <li><span>Subtotal</span> <span>{{Cart::subtotal(0)}}</span></li>
                    <li><span>Shipping</span> <span>Free</span></li>
                    <li><span>Total</span> <span>{{Cart::total(0)}}</span></li>
                </ul>

                <div id="accordion" role="tablist" class="mb-4">
                    <div class="card">
                        <div class="card-header" role="tab" id="headingOne">
                            <h6 class="mb-0">
                                <a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><i class="fa fa-circle-o mr-3"></i>Paypal</a>
                            </h6>
                        </div>

                        <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pharetra tempor so dales. Phasellus sagittis auctor gravida. Integ er bibendum sodales arcu id te mpus. Ut consectetur lacus.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingTwo">
                            <h6 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="fa fa-circle-o mr-3"></i>cash on delievery</a>
                            </h6>
                        </div>
                        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo quis in veritatis officia inventore, tempore provident dignissimos.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingThree">
                            <h6 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class="fa fa-circle-o mr-3"></i>credit card</a>
                            </h6>
                        </div>
                        <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse quo sint repudiandae suscipit ab soluta delectus voluptate, vero vitae</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingFour">
                            <h6 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour"><i class="fa fa-circle-o mr-3"></i>direct bank transfer</a>
                            </h6>
                        </div>
                        <div id="collapseFour" class="collapse show" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est cum autem eveniet saepe fugit, impedit magni.</p>
                            </div>
                        </div>
                    </div>
                </div>
                @if(Cart::count()>0)
                <a href="/checkout" class="btn essence-btn">Check out</a>
                    @endif
            </div>
        </div>

    </div>

</div>

@endsection

@section('extra-js')
    <script src="{{asset('js/app.js')}}"></script>
    <script>
        (function () {
            const classname=document.querySelectorAll('.qty')
            Array.from(classname).forEach(function (element) {
                element.addEventListener('change',function () {
                    const id = element.getAttribute('data-id')
                    axios.patch(`/cart/${id}`, {
                        quantity:this.value
                    })
                        .then(function (response) {
                            // console.log(response);
                            window.location.href='{{route('cart.index')}}'
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                })

            })
        })()
    </script>
    @endsection
