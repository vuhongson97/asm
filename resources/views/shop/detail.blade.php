@extends('layouts.master')
@section('title','detail')
@section('content')
<!-- ##### Single Product Details Area Start ##### -->
<section class="single_product_details_area d-flex align-items-center">

    <!-- Single Product Thumb -->
    <div class="single_product_thumb clearfix">
        <div class="product_thumbnail_slides owl-carousel">

            @foreach( json_decode($pro->image) as $img)
            <img src="{{asset(Voyager::image($img))}}" alt="{{$pro->name}}" width="400px">

                @endforeach
        </div>
    </div>

    <!-- Single Product Description -->
    <div class="single_product_desc clearfix">
        <span>{{$cat->name}}</span>


        <a href="cart.html">
            <h2>{{$pro->name}}</h2>
        </a>
        @if($pro->discount>0)
            @php
                $newprice=$pro->price-($pro->price*($pro->discount/100))
            @endphp
            <p class="product-price"><span
                    class="old-price">{{number_format($pro->price)}}</span> {{number_format($newprice)}}
            </p>
            @php
                $price=$newprice
            @endphp
        @else
            @php
                $price=$pro->price
            @endphp
            <p class="product-price"> {{number_format($pro->price)}}
            </p>
        @endif
        <span>{{$brand->name}}</span>
        <p class="product-desc"><?=$pro->short_detail?></p>

        <!-- Form -->
        <form class="cart-form clearfix" method="post">
            <!-- Select Box -->
            <div class="select-box d-flex mt-50 mb-30">


            </div>
            <!-- Cart & Favourite Box -->
            <div class="cart-fav-box d-flex align-items-center">
                <!-- Cart -->
                <form action="{{Route('cart.store')}}" method="post">
                    {{csrf_field()}}


                        <button class=" btn btn-decrement btn-outline-secondary sub" type="button">-</button>

                        <input type="number" name="qty" id="1" value="1" min="1" max="50" class="form-control w-25 text-center"/>

                        <button class="btn btn-increment btn-outline-secondary add  mr-30" type="button">+</button>



                    <input type="hidden" name="id" value="{{$pro->id}}">
                    <input type="hidden" name="name" value="{{$pro->name}}">
                    <input type="hidden" name="price" value="{{$price}}">
                    <button type="submit" class="btn essence-btn">Add to cart</button>
                </form>
                <!-- Favourite -->
                <div class="product-favourite ml-4">
                    <a href="#" class="favme fa fa-heart"></a>
                </div>
            </div>
        </form>
    </div>
</section>
<section>
    <div class="container">
        <h5 class="text-center text-uppercase">Chi tiết sản phẩm</h5>
        <br>
        <?=$pro->detail?>
    </div>
</section>
<!-- ##### Single Product Details Area End ##### -->
@endsection
@section('extra-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script>

        $('.add').click(function () {
            if ($(this).prev().val() < 50) {
                $(this).prev().val(+$(this).prev().val() + 1);
            }
        });
        $('.sub').click(function () {
            if ($(this).next().val() > 1) {
                if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
            }
        });


    </script>
    @endsection
