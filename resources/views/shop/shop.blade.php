@extends('layouts.master')
@section('title','Shop')
@section('content')
    <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb_area bg-img" style="background-image: url(img/bg-img/breadcumb.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h2>{{$categoryName}}</h2>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Shop Grid Area Start ##### -->
    <section class="shop_grid_area section-padding-80">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4 col-lg-3">
                    <div class="shop_sidebar_area">

                        <!-- ##### Single Widget ##### -->
                        <div class="widget catagory mb-50">
                            <!-- Widget Title -->
                            <h6 class="widget-title mb-30">Catagories</h6>

                            <!--  Catagories  -->
                            <div class="catagories-menu">
                                <ul id="menu-content2" class="menu-content collapse show">
                                    <!-- Single Item -->
                                    @foreach($cat as $cat)
                                    <li>
                                    <a href="{{route('shop.index',['category'=>$cat->id])}}">{{$cat->name}}</a>

                                    </li>
                                    <!-- Single Item -->
                                    @endforeach


                                </ul>
                            </div>
                        </div>

                        <!-- ##### Single Widget ##### -->
                        {{-- <div class="widget price mb-50">
                            <!-- Widget Title -->
                            <h6 class="widget-title mb-30">Filter by</h6>
                            <!-- Widget Title 2 -->
                            <p class="widget-title2 mb-30">Price</p>

                            <div class="widget-desc">
                                <div class="slider-range">
                                    <div data-min="49" data-max="360" data-unit="$"
                                         class="slider-range-price ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"
                                         data-value-min="49" data-value-max="360" data-label-result="Range:">
                                        <div class="ui-slider-range ui-widget-header ui-corner-all"></div>
                                        <span class="ui-slider-handle ui-state-default ui-corner-all"
                                              tabindex="0"></span>
                                        <span class="ui-slider-handle ui-state-default ui-corner-all"
                                              tabindex="0"></span>
                                    </div>
                                    <div class="range-price">Range: $49.00 - $360.00</div>
                                </div>
                            </div>
                        </div> --}}


                        <!-- ##### Single Widget ##### -->
                        <div class="widget brands mb-50">
                            <!-- Widget Title 2 -->
                            <p class="widget-title2 mb-30">Brands</p>
                            <div class="widget-desc">
                                <ul>
                                    @foreach($brands as $brand)
                                <li><a href="{{route('shop.index',['category'=>request()->category,'brand'=>$brand->id])}}">{{$brand->name}}</a></li>
                                    @endforeach


                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-8 col-lg-9">
                    <div class="shop_grid_product_area">
                        <div class="row">
                            <div class="col-12">
                                <div class="product-topbar d-flex align-items-center justify-content-between">
                                    <!-- Total Products -->
                                    <div class="total-products">
                                        <p><span>{{$count}}</span> products found</p>
                                    </div>
                                    <!-- Sorting -->
                                    <div class="product-sorting d-flex">
                                        <p>Sort by:</p>
                                            <span>
                                            <a href="{{route('shop.index',['category'=>request()->category,'brand'=>$brand->id, 'sort'=>'H_L'])}}">Hight - Low</a> |
                                            <a href="{{route('shop.index',['category'=>request()->category,'brand'=>$brand->id, 'sort'=>'L_H'])}}">Low - Hight</a>
                                            </span>


                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                        @foreach($pros as $pro)
                            @php $pic=json_decode($pro->image)@endphp
                            <!-- Single Product -->
                                <a href="/detail/{{$pro->id}}/{{covert_vi_to_en($pro->name)}}">
                                <div class="col-12 col-sm-6 col-lg-4">
                                    <div class="single-product-wrapper">
                                        <!-- Product Image -->
                                        <div class="product-img">
                                            <img src="{{asset(Voyager::image($pic[0]))}}" alt="{{$pro->name}}">
                                            <!-- Hover Thumb -->
                                            <img class="hover-img" src="{{asset(Voyager::image($pic[1]))}}"
                                                 alt="{{$pro->name}}">
                                            @if($pro->discount>0)
                                            <!-- Product Badge -->
                                            <div class="product-badge offer-badge">
                                                <span>-{{$pro->discount}}%</span>
                                            </div>
                                            @endif
                                            <!-- Favourite -->
                                            <div class="product-favourite">
                                                <a href="#" class="favme fa fa-heart"></a>
                                            </div>
                                        </div>

                                        <!-- Product Description -->
                                        <div class="product-description">

                                            <a href="/detail/{{$pro->id}}/{{covert_vi_to_en($pro->name)}}">
                                                <h6>{{$pro->name}}</h6>
                                            </a>
                                            @if($pro->discount>0)
                                                @php
                                                    $newprice=$pro->price-($pro->price*($pro->discount/100))
                                                @endphp
                                                <p class="product-price"><span
                                                        class="old-price">{{number_format($pro->price)}}</span> {{number_format($newprice)}}
                                                </p>
                                               @php
                                               $price=$newprice
                                               @endphp
                                        @else
                                                <p class="product-price"> {{number_format($pro->price)}}
                                                </p>
                                                @php
                                                $price=$pro->price
                                                @endphp
                                            @endif
                                        <!-- Hover Content -->
                                            <div class="hover-content">
                                                <!-- Add to Cart -->
                                                <div class="add-to-cart-btn">
{{--                                                    <a href="#" class="btn essence-btn">Add to Cart</a>--}}
                                                    <form action="{{Route('cart.store')}}" method="post">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="id" value="{{$pro->id}}">
                                                        <input type="hidden" name="name" value="{{$pro->name}}">
                                                        <input type="hidden" name="price" value="{{$price}}">
                                                        <button type="submit" class="btn essence-btn">Add to cart</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            @endforeach


                        </div>
                    </div>
                    <!-- Pagination -->
                    <nav aria-label="navigation">
                        <ul class="pagination mt-50 mb-70">
                            {{-- <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-left"></i></a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">...</a></li>
                            <li class="page-item"><a class="page-link" href="#">21</a></li>
                            <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-right"></i></a>
                            </li> --}}

                            {{-- {{$pros->links()}} --}}
                            {{$pros->appends(request()->input())->links()}}

                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Shop Grid Area End ##### -->
@endsection

