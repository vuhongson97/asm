<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// trang chủ
Route::get('/', 'homeController@index')->name('home');
//trang shop

Route::get('/shop', 'shopController@index')->name('shop.index');
// chi tiet SP
Route::get('/detail/{id}/{name}', 'shopController@detail');
//add cart
Route::post('/detail/{id}/{name}', 'CartController@store')->name('cart.store');
//lien he
Route::get('/contact', function () {
    return view('contact');
});

Route::get('/checkout', function () {
    return view('shop.checkout');
})->middleware('auth');
//add DB
Route::post('/checkout', 'checkoutController@store')->name('addDB');
// show cart
Route::get('/cart', 'CartController@index')->name('cart.index');
// add cart
Route::post('/shop', 'CartController@store')->name('cart.store');
// delete cart
Route::get('/delete/{id}', 'CartController@destroy')->name('cart.destroy');
//update cart
Route::patch('/cart/{id}', 'CartController@update')->name('cart.update');

// destroy cart
Route::get('/empty', function () {
    Cart::destroy();
    return back();
});
//search
route::get('/search', 'shopController@search')->name('search');

// admin
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
// login
Auth::routes();

// home
Route::get('/home', 'HomeController@index')->name('home');

//profileblade
Route::get('/profile', function () {
    return view('profile');
})->name('profile');
